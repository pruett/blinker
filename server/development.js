const serve = require("webpack-serve");
const webpackServeWaitpage = require("webpack-serve-waitpage");
const config = require("../webpack.config.js");

serve({
    config,
    add: (app, middleware, options) => {
        app.use(
            webpackServeWaitpage(options, {
                title: "Blinker.com Coding Challenge",
                theme: "dark",
            })
        );
    },
    hot: {
        host: "localhost",
        port: 8090,
    },
});
