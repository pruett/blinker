# Coding Challenge

## Getting started

To run dev server on localhost:8080:

```bash
yarn dev
```

To run minified build:

```bash
open dist/index.html
```

## To-Do

- [x] Using this JSON endpoint, create a List that shows each vehicle's `year`, `make`, and `model` (1999 Honda Accord) in each item, along with its mileage.

- [x] Include sorting functionality that can sort the vehicle list from oldest to newest by vehicle year, highest to lowest mileage, and newest to oldest listing date

- [x] Include a search field at the top of the screen to filter vehicles in the list.

- [x] Clicking on a vehicle should take you to a basic detail page listing out the vehicle details(Year, Make, Model, Mileage, and Image)

- [x] Have basic styling but should be responsive from mobile screens to a desktop experience.

- [x] Write some unit tests. Not looking for 100% code coverage, just interested in how you approach testing.


## Requirements

> You may use any Front End libraries/frameworks/build tools, let your creativity run wild.

I decided to go with React as the ecosystem is rich and mature. I was considering using something like Elm (a lot of experience) or ReasonML (no experience), but thought React was a good fit here. Typescript is another intriguing tool that I considered, but ultimately chose against due to my limited experience.

#### State management

Ultimately I decided to use React's built-in component state as opposed to a library like redux. React's component state was sufficient in this small example although I do feel a library like redux can make state management a bit more testable and reliable. Another thought was to use React's newer public Context API, but I haven't had experience as of yet. Using the `<Provider>` `<Consumer>` pattern looks interesting and something I'll continue looking into.

#### Folder structure

I chose the names `Components` and `Containers` to house the UI modules where `Containers` represents those which are "stateful" and `Components` which are essentially "stateless" rendering functions that consume state as props.

#### Styles

I'm using webpack + css modules to load the styles via JavaScript.

#### Testing

I'm using Jest here, testing the sorting functionality.

> You may use whatever resources you'd like to complete the challenge, such as 3rd-party libraries, Google, or StackOverflow.

I pulled in lodash's `sortby` package as well as `fuse.js` fuzzy search library.
