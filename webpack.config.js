const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    mode: "development",
    entry: { main: "./src/index.js" },
    output: {
        path: path.resolve(__dirname, "dist"), // eslint-disable-line
        filename: "[name].bundle.js",
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [require.resolve("babel-loader")],
            },
            {
                test: /\.css$/,
                exclude: /node_modules/,
                use: [
                    { loader: require.resolve("style-loader") },
                    {
                        loader: require.resolve("css-loader"),
                        options: {
                            modules: true,
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "Blinker x Kevin Pruett | Coding Challenge",
            template: path.resolve(__dirname, "templates/index.html"), // eslint-disable-line
        }),
    ],
};
