import React from "react";
import ReactDOM from "react-dom";
import App from "./Containers/App.js";

const initialProps = {
    endpoint:
        "https://gist.githubusercontent.com/creatifyme/2a334c00a117097bfdb47f031edf292c/raw/efb52ecf1cf92e2261f504ec7639c68b5ff390bd/cars.json",
    artificialDelay: 2500,
};

ReactDOM.render(<App {...initialProps} />, document.getElementById("app"));
