import orderBy from "lodash.orderby";

const year = (order = "DESC", data) => {
    switch (order) {
        case "ASC":
            return orderBy(data, ["year"], ["asc"]);
        case "DESC":
            return orderBy(data, ["year"], ["desc"]);
        default:
            throw new Error(
                "Invalid Argument: Provide either 'ASC' or 'DESC' as order argument"
            );
    }
};

const mileage = (order = "DESC", data) => {
    switch (order) {
        case "ASC":
            return orderBy(data, ["mileage"], ["asc"]);
        case "DESC":
            return orderBy(data, ["mileage"], ["desc"]);
        default:
            throw new Error(
                "Invalid Argument: Provide either 'ASC' or 'DESC' as order argument"
            );
    }
};

const listing = (order, data) => {
    switch (order) {
        case "ASC":
            return orderBy(data, ["created_at"], ["asc"]);
        case "DESC":
            return orderBy(data, ["created_at"], ["desc"]);
        default:
            throw new Error(
                "Invalid Argument: Provide either 'ASC' or 'DESC' as order argument"
            );
    }
};

export { year, mileage, listing };
