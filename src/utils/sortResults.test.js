import { year, mileage, listing } from "./sortResults.js";

const kia = {
    year: 2013,
    mileage: 24235,
    created_at: "2016-10-14T20:13:22.586Z",
};

const nissan = {
    year: 2014,
    mileage: 10457,
    created_at: "2016-10-14T20:13:22.586Z",
};

const landrover = {
    year: 2014,
    mileage: 7458,
    created_at: "2016-12-14T20:13:22.586Z",
};

const jag = {
    year: 2014,
    mileage: 9852,
    created_at: "2016-10-14T20:13:22.586Z",
};

const audi = {
    year: 2013,
    mileage: 17216,
    created_at: "2016-10-14T20:13:22.586Z",
};

const jeep = {
    year: 2013,
    mileage: 19000,
    created_at: "2016-10-14T20:13:22.586Z",
};

const bmw = {
    year: 1999,
    mileage: 160254,
    created_at: "2016-10-14T20:13:22.586Z",
};

const lincoln = {
    year: 2016,
    mileage: 1545,
    created_at: "2016-10-14T20:13:22.586Z",
};

const ram = {
    year: 2015,
    mileage: 10547,
    created_at: "2016-10-14T20:13:22.586Z",
};

const testData = [kia, nissan, landrover, jag, audi, jeep, bmw, lincoln, ram];

describe("Sorting inventory", () => {
    describe("Year", () => {
        test("ASC should yield oldest cars first", () => {
            expect(year("ASC", testData)).toEqual([
                bmw,
                kia,
                audi,
                jeep,
                nissan,
                landrover,
                jag,
                ram,
                lincoln,
            ]);
        });

        test("DESC should yield newest cars first", () => {
            expect(year("DESC", testData)).toEqual([
                lincoln,
                ram,
                nissan,
                landrover,
                jag,
                kia,
                audi,
                jeep,
                bmw,
            ]);
        });

        test("providing invalid order argument should yield an error", () => {
            expect(() => year("foo", testData)).toThrow(/^Invalid/);
        });
    });

    describe("Mileage", () => {
        test("ASC should yield cars from lowest mileage to highest", () => {
            expect(mileage("ASC", testData)).toEqual([
                lincoln,
                landrover,
                jag,
                nissan,
                ram,
                audi,
                jeep,
                kia,
                bmw,
            ]);
        });

        test("DESC should yield cars from highest mileage to lowest", () => {
            expect(mileage("DESC", testData)).toEqual([
                bmw,
                kia,
                jeep,
                audi,
                ram,
                nissan,
                jag,
                landrover,
                lincoln,
            ]);
        });

        test("providing invalid order argument should yield an error", () => {
            expect(() => mileage("foo", testData)).toThrow(/^Invalid/);
        });
    });

    describe("Listing date", () => {
        test("ASC should yield cars from earliest listing date to most recent", () => {
            expect(listing("ASC", testData)).toEqual([
                kia,
                nissan,
                jag,
                audi,
                jeep,
                bmw,
                lincoln,
                ram,
                landrover,
            ]);
        });

        test("DESC should yield cars from most recent to oldest listing date", () => {
            expect(listing("DESC", testData)).toEqual([
                landrover,
                kia,
                nissan,
                jag,
                audi,
                jeep,
                bmw,
                lincoln,
                ram,
            ]);
        });

        test("providing invalid order argument should yield an error", () => {
            expect(() => mileage("foo", testData)).toThrow(/^Invalid/);
        });
    });
});
