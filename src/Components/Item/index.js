import React from "react";
import styles from "./styles.css";

export default ({ item, onSelectItem }) => (
    <li>
        <ul
            className={styles.itemContainer}
            onClick={onSelectItem.bind(this, item)}
        >
            <li className={styles.item}>
                <span className={styles.category}>Make</span> {item.make}
            </li>
            <li className={styles.item}>
                <span className={styles.category}>Model</span> {item.model}
            </li>
            <li className={styles.item}>
                <span className={styles.category}>Year</span> {item.year}
            </li>
            <li className={styles.item}>
                <span className={styles.category}>Mileage</span> {item.mileage}
            </li>
        </ul>
    </li>
);
