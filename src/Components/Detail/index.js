import React from "react";
import styles from "./styles.css";

export default ({ item, clearItemSelection }) => (
    <div>
        <button onClick={clearItemSelection} className={styles.backButton}>
            ← back to results
        </button>
        <img className={styles.image} src={item.image_url} />
        <h1>{`${item.year} ${item.make} ${item.model}`}</h1>
        <h2>Details</h2>
        <ul>
            <li>Bodytype: {item.bodytype}</li>
            <li>Drivetrain: {item.drivetrain}</li>
            <li>Mileage: {item.mileage}</li>
        </ul>
    </div>
);
