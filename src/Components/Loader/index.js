import React from "react";
import styles from "./styles.css";

export default () => (
    <div className={styles.container}>
        <h1>Loading inventory...</h1>
        <img
            className={styles.spinner}
            src="https://s3-us-west-2.amazonaws.com/blinker-brochure-production/wp-content/uploads/2016/09/08211847/spinner.png"
        />
    </div>
);
