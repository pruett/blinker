import React from "react";
import styles from "./styles.css";

export default ({ onSortChange }) => (
    <div className={styles.container}>
        <span className={styles.label}>Sort by:</span>
        <select onChange={onSortChange}>
            <option value="listing_desc">Listing: Newest to Oldest</option>
            <option value="listing_asc">Listing: Oldest to Newest</option>
            <option value="year_desc">Year: Newest to Oldest</option>
            <option value="year_asc">Year: Oldest to Newest</option>
            <option value="mileage_desc">Mileage: High to Low</option>
            <option value="mileage_asc">Mileage: Low to High</option>
        </select>
    </div>
);
