import React from "react";
import styles from "./styles.css";

export default ({ searchQuery, clearSearchResults }) => (
    <div>
        <div className={styles.searchContainer}>
            <h2 className={styles.headline}>
                {"Displaying results for "}
                <span className={styles.searchQuery}>{searchQuery}</span>
            </h2>
            <button className={styles.clearButton} onClick={clearSearchResults}>
                ⨯
            </button>
        </div>
    </div>
);
