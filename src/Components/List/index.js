import React from "react";
import Item from "../Item";
import SearchForm from "../Search/form.js";
import SearchHeadline from "../Search/headline.js";
import Sort from "../Sort";
import styles from "./styles.css";

export default ({
    allInventory,
    clearSearchResults,
    onSearchChange,
    onSearchSubmit,
    onSelectItem,
    onSortChange,
    searchInput,
    searchQuery,
    searchResults,
}) => {
    const hasSearchResults = searchResults.length > 0;

    return (
        <div>
            <SearchForm
                searchInput={searchInput}
                onSearchChange={onSearchChange}
                onSearchSubmit={onSearchSubmit}
            />
            {hasSearchResults && (
                <SearchHeadline
                    clearSearchResults={clearSearchResults}
                    searchQuery={searchQuery}
                />
            )}
            <Sort onSortChange={onSortChange} />
            {hasSearchResults ? (
                <ul className={styles.listContainer}>
                    {searchResults.map((item, i) => (
                        <Item
                            key={`searchItem${i}`}
                            item={item}
                            onSelectItem={onSelectItem}
                        />
                    ))}
                </ul>
            ) : (
                <ul className={styles.listContainer}>
                    {allInventory.map((item, i) => (
                        <Item
                            key={`inventoryItem${i}`}
                            item={item}
                            onSelectItem={onSelectItem}
                        />
                    ))}
                </ul>
            )}
        </div>
    );
};
