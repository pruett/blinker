import React, { Component } from "react";
import Fuse from "fuse.js";
import Loader from "../Components/Loader";
import List from "../Components/List";
import Detail from "../Components/Detail";
import { year, mileage, listing } from "../utils/sortResults.js";

export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allInventory: null,
            fetching: true,
            searchInput: "",
            searchQuery: "",
            searchResults: [],
            selectedItem: null,
        };
        this.clearSearchResults = this.clearSearchResults.bind(this);
        this.clearItemSelection = this.clearItemSelection.bind(this);
        this.onSearchChange = this.onSearchChange.bind(this);
        this.onSearchSubmit = this.onSearchSubmit.bind(this);
        this.onSelectItem = this.onSelectItem.bind(this);
        this.onSortChange = this.onSortChange.bind(this);
    }

    componentDidMount() {
        this.fetchData(this.props.endpoint);
    }

    onSearchChange(evt) {
        this.setState({ searchInput: evt.target.value });
    }

    onSearchSubmit(evt) {
        evt.preventDefault();
        this.setState({
            searchQuery: this.state.searchInput,
            searchResults: this.search(this.state.searchInput),
        });
    }

    onSelectItem(item) {
        this.clearSearchResults();
        this.setState({
            selectedItem: item,
        });
    }

    search(query) {
        const opts = {
            keys: ["make", "model", "year"],
            threshold: 0.3,
        };
        const searchable = new Fuse(this.state.allInventory, opts);
        return searchable.search(query);
    }

    onSortChange(evt) {
        this.state.searchResults.length > 1
            ? this.setState({
                  searchResults: this.sortResults(
                      evt,
                      this.state.searchResults
                  ),
              })
            : this.setState({
                  allInventory: this.sortResults(evt, this.state.allInventory),
              });
    }

    sortResults(evt, data) {
        switch (evt.target.value) {
            case "listing_desc":
                return listing("DESC", data);
            case "listing_asc":
                return listing("ASC", data);
            case "mileage_desc":
                return mileage("DESC", data);
            case "mileage_asc":
                return mileage("ASC", data);
            case "year_desc":
                return year("DESC", data);
            case "year_asc":
                return year("ASC", data);
        }
    }

    clearSearchResults() {
        this.setState({
            searchInput: "",
            searchQuery: "",
            searchResults: [],
        });
    }

    clearItemSelection() {
        this.setState({
            selectedItem: null,
        });
    }

    fetchData(endpoint) {
        setTimeout(() => {
            fetch(endpoint)
                .then(res => {
                    return res.json();
                })
                .then(data => {
                    this.setState({
                        fetching: false,
                        allInventory: data,
                    });
                });
        }, this.props.artificialDelay || 0);
    }

    render() {
        return this.state.fetching ? (
            <Loader />
        ) : this.state.selectedItem ? (
            <Detail
                item={this.state.selectedItem}
                clearItemSelection={this.clearItemSelection}
            />
        ) : (
            <List
                allInventory={this.state.allInventory}
                clearSearchResults={this.clearSearchResults}
                onSearchChange={this.onSearchChange}
                onSearchSubmit={this.onSearchSubmit}
                onSelectItem={this.onSelectItem}
                onSortChange={this.onSortChange}
                searchInput={this.state.searchInput}
                searchQuery={this.state.searchQuery}
                searchResults={this.state.searchResults}
            />
        );
    }
}
